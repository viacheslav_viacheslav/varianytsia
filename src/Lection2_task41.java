import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by varianytsia on 06-Apr-16.
 */
public class Lection2_task4
{
    public static void main(String args[]) throws IOException
    {
        System.out.println("Введите число");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String readerString1 = reader.readLine();
        float f1 = Float.parseFloat(readerString1);






        System.out.println("Введите знак математической операции");
        String readerStringMathOp = reader.readLine();
        char readerStringMathOp1 = readerStringMathOp.charAt(0);



        System.out.println("Введите второе число");
        String readerString2 = reader.readLine();
        float f2 = Float.parseFloat(readerString2);



        switch (readerStringMathOp1){


            case ('+'):{
                boolean intOrFloat = (f1%1==0)&&(f2%1==0);
                if (intOrFloat){
                    System.out.println("Ответ  " + ((int)(f1+f2)));
                }
                else
                System.out.println(f1+f2);
                break;
            }
            case ('-'):{
                boolean intOrFloat = (f1%1==0)&&(f2%1==0);
                if (intOrFloat){
                    System.out.println("Ответ  " + ((int)(f1-f2)));
                }
                else
                    System.out.println(f1-f2);
                break;
            }
            case ('*'):{
                boolean intOrFloat = (f1%1==0)&&(f2%1==0);
                if (intOrFloat){
                    System.out.println("Ответ  " + ((int)(f1*f2)));
                }
                else
                    System.out.println(f1*f2);
                break;
            }
            case ('/'):{
                boolean intOrFloat = (f1%1==0)&&(f2%1==0);
                if (intOrFloat){
                    System.out.println("Ответ  " + ((int)(f1/f2)));
                }
                else
                    System.out.println(f1/f2);
                break;
            }
            case ('%'):{
                boolean intOrFloat = (f1%1==0)&&(f2%1==0);
                if (intOrFloat){
                    System.out.println("Ответ  " + ((int)(f1%f2)));
                }
                else
                    System.out.println(f1%f2);
                break;
            }
            default:
                System.out.println("Что-то пошло не так");
        }


    }
}
