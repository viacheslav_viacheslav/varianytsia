import junit.framework.Assert;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


import java.util.concurrent.TimeUnit;

/**
 * Created by varianytsia on 26-May-16.
 */
public class TestRozetka {

    public static  WebDriver driver;
    public static  String URL;


    @BeforeClass
    public static void initialize(){
        driver = new FirefoxDriver();
        URL = "http://rozetka.com.ua/";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
    @Before
    public void openBrowser() {

        driver.get(URL);

    }

    @Test
    public void testLogo() {

        Assert.assertTrue("Логотип не отображается", driver.findElement(By.xpath("//*[@id='body-header']//img[contains(@alt, 'Rozetka.ua')]")).isDisplayed());

    }

    @Test
    public void testApple() {
        Assert.assertTrue("Эппла нету",driver.findElement(By.xpath("//*[@id='m-main']")).getText().contains("Apple"));
    }

    @Test
    public void testMP3() {
        Assert.assertTrue("MP3 нету",driver.findElement(By.xpath("//*[@id='m-main']")).findElement(By.xpath("//*[@id='m-main']/li[3]/a")).getText().contains("MP3"));
    }

    @Test
    public void testCity() {
        driver.findElement(By.xpath("//*[@id='city-chooser']/a")).click();
        Assert.assertTrue("Харьков не отображается",driver.findElement(By.xpath("//*[@id='city-chooser']//*[contains(@class,'header-city')][contains(text(),'Харьков')]")).isDisplayed());
        Assert.assertTrue("Киев не отображается",driver.findElement(By.xpath("//*[@id='city-chooser']//*[contains(@class,'header-city')][contains(text(),'Киев')]")).isDisplayed());
        Assert.assertTrue("Одесса не отображается",driver.findElement(By.xpath("//*[@id='city-chooser']//*[contains(@class,'header-city')][contains(text(),'Одесса')]")).isDisplayed());

    }

    @Test
    public void testBin(){
        driver.findElement(By.xpath("//a[@class='sprite-side novisited hub-i-link hub-i-cart-link']")).click();
        Assert.assertTrue("Корзина не пуста",driver.findElement(By.xpath("//*[@id='drop-block']/h2")).getText().contains("Корзина пуста"));
    }


    @After
    public void end() {

    }

    @AfterClass
    public static  void completelyEnd(){
        driver.quit();
    }


}
