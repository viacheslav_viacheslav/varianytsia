import junit.framework.Assert;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by varianytsia on 27-May-16.
 */
public class TestStackoverflow {
    public static WebDriver driver;
    public static String URL;

    @BeforeClass
    public static void initialize() {
        driver = new FirefoxDriver();
        URL = "http://stackoverflow.com/";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Before
    public void openBrowser() {
        driver.get(URL);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Test
    public void countTest() {
        String a = driver.findElement(By.xpath("//*[@id='tabs']//*[span]/span")).getText();
        //System.out.println(a);
        int b = Integer.parseInt(a);
        Assert.assertTrue("Здесь меньше 300 вопросов",b > 300);
    }


    @Test
    public void socialNetworksTest() {
        driver.findElement(By.xpath("//*[@id='tell-me-more']")).click();
        //System.out.println(driver.getCurrentUrl());
        Assert.assertTrue("Гугл не отображается",driver.findElement(By.xpath("//*[@id='openid-buttons']//div[contains(@data-provider,'google')]")).isDisplayed());
        Assert.assertTrue("Фейсбук не отображается",driver.findElement(By.xpath("//*[@id='openid-buttons']//div[contains(@data-provider,'facebook')]")).isDisplayed());
    }

    @Test
    public void questionDateTest() throws InterruptedException, ParseException {
        driver.findElement(By.xpath("//a[@class='question-hyperlink']")).click();
        //Thread.sleep(10000);
        //driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);
        String a = driver.findElement(By.xpath("//*[@id='question']//span[@class='relativetime']")).getAttribute("title");
        //System.out.println(a);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date b = sdf.parse(a);
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        Assert.assertEquals("Вопрос был задан не сегодня",sdf2.format(b), sdf2.format(today));
        //System.out.println(sdf2.format(b));
        //System.out.println(sdf2.format(today));
    }

    @Test
    public void kTest() {
        List<WebElement> list = new ArrayList<WebElement>();
        list = driver.findElements(By.xpath("//*[@id='hireme']//ul[@class='jobs']/descendant::*[contains(text(),'$')]"));
        List<Integer> intList = new ArrayList<Integer>();
//        System.out.println("size = " + list.size());
        Pattern pattern = Pattern.compile("[0-9]+");

//        for (WebElement s : list) {
//            System.out.println(s.getText());
//        }
        for (WebElement s : list) {
            int b=0;
            Matcher matcher = pattern.matcher(s.getText());
            while (matcher.find()) {
                String a = matcher.group();
                //System.out.println(a);
                b = Integer.parseInt(a);
               // System.out.println(b);
//                Assert.assertTrue(b>100);

            }
            intList.add(b);
        }
        boolean bool = false;
        for (Integer i : intList){
//            System.out.println(i);
            if (i >= 100){
                bool = true;
                break;
            }
        }

        Assert.assertTrue("Нету зарплат больше $100K",bool);

    }

    @After
    public void end() {

    }

    @AfterClass
    public static void completelyEnd() {
        driver.quit();
    }


}
