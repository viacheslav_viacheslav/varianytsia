import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by varianytsia on 31-May-16.
 */
public class TestStackoverflowPageObject {
    public static WebDriver driver;
    public static StackoverflowStartPageMap stackoverflowStartPageMap;
    public static StackoverflowNetworkPage socialNetworkPage;
    public static StackoverflowQuestionPage questionPage;
    public static String URL;

    @BeforeClass
    public static void initialize() {
        driver = new FirefoxDriver();
        URL = "http://stackoverflow.com/";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        stackoverflowStartPageMap = new StackoverflowStartPageMap(driver);
    }

    @Before
    public void openBrowser() {
        if (driver == null) {
            driver = new FirefoxDriver();
        }
        driver.get(URL);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void end() {
        driver.close();
    }

    @Test
    public void countTest() {
        String a = stackoverflowStartPageMap.featuredCount.getText();
        int b = Integer.parseInt(a);
        assertTrue("Число не более 300", b > 300);
    }

    @Test
    public void socialNetworktest() {
        socialNetworkPage = stackoverflowStartPageMap.signUpClick();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        assertTrue("Фэйсбук не отображается", socialNetworkPage.FaceBook.isDisplayed());
        assertTrue("Гугл не отображается", socialNetworkPage.Google.isDisplayed());
    }

    @Test
    public void questionDateTest() throws ParseException {
        questionPage = stackoverflowStartPageMap.questionClick();
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        assertEquals("Вопрос был задан не сегодня", sdf2.format(questionPage.getDateOfQuestion()), sdf2.format(today));
    }

    @Test
    public void testOffers(){
        assertTrue("Нету зарплат больше 100 килобаксов",stackoverflowStartPageMap.areThereJobsOver100(stackoverflowStartPageMap.jobOffers));
    }


}
