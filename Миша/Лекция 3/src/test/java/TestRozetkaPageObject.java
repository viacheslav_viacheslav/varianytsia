import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;


/**
 * Created by varianytsia on 30-May-16.
 */
public class TestRozetkaPageObject {
    public static WebDriver driver;
    public static RozetkaMap rozetkaMap;
    public static String URL;

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        URL = "http://rozetka.com.ua/";
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        rozetkaMap = new RozetkaMap(driver);
    }

    @Before
    public void start() throws Exception {
        if (driver == null) {
            driver = new FirefoxDriver();
        }
        driver.get(URL);
    }

    @AfterClass
    public static void theEnd() {
        driver.close();
    }


    @Test
    public void checkLogo() {
        assertTrue("Розетка не отображается", rozetkaMap.Logo.isDisplayed());
    }

    @Test
    public void testApple() {
        assertTrue("Эппл не отображается", rozetkaMap.AppleText.isDisplayed());
    }

    @Test
    public void testMP3() {
        assertTrue("МР3 не отображается", rozetkaMap.MP3.isDisplayed());
    }

    @Test
    public void testCities() {
        assertTrue("Харьков не отображается", rozetkaMap.clickCityPicker().Kharkov.isDisplayed());
        assertTrue("Харьков не отображается", rozetkaMap.clickCityPicker().Kiev.isDisplayed());
        assertTrue("Харьков не отображается", rozetkaMap.clickCityPicker().Odessa.isDisplayed());
    }

    @Test
    public void testBin(){
        assertTrue("Корзина не пуста",rozetkaMap.clickBin().KorzinaPustaText.isDisplayed());
    }



}
