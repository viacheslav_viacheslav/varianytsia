package StackoverflowBDD.runner;

import StackoverflowBDD.stepDefinitions.StackoverflowNetworkPage;
import StackoverflowBDD.stepDefinitions.StackoverflowQuestionPage;
import StackoverflowBDD.stepDefinitions.StackoverflowStartPageMap;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by varianytsia on 02-Jun-16.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/cucumber-report/stackoverflow", "json:target/cucumber.json"},
        features = "src/test/java/StackoverflowBDD/features",
        glue = "StackoverflowBDD/stepDefinitions",
        tags = "@NeededTest")

//@NeededTest - запускает 2 теста из стэковерфлоу
//@stackoverflow - запускает все тесты стэковерфлоу



public class Run {
    public static WebDriver driver;
    public static String URL;
    public static StackoverflowStartPageMap stackoverflowStartPageMap;
    public static StackoverflowNetworkPage stackoverflowNetworkPage;
    public static StackoverflowQuestionPage stackoverflowQuestionPage;

    @BeforeClass
    public static void setUp(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        stackoverflowStartPageMap = new StackoverflowStartPageMap(driver);
        URL="http://stackoverflow.com/";
    }

    @AfterClass
    public static void end(){
        driver.close();
    }

}
