@stackoverflow
Feature: Check the functionality of stackoverflow

  Background:
    Given I navigate to Stackoverflow start page

  Scenario: 001 Checking 'featured' quantity
    Then I see the that featured quantity is more than 300 on start page
  @NeededTest
  Scenario Outline: 002 Checking social networks
    When I click on 'Sign Up' button on Stackoverflow start page
    Then I see "<networks>" buttons on the network page

    Examples:
      | networks |
      | Google   |
      | Facebook |

  @NeededTest
  Scenario: 003 Checking the date of any question
    When I click on any question on Stackoverflow start page
    Then I see that question has today's date on question page

  Scenario: 004 Checking job offers
    Then I see that job offers with more than $ 100 K are present on Stackoverflow start page


