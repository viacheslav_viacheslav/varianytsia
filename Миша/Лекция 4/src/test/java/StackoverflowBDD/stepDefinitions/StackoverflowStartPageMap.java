package StackoverflowBDD.stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by varianytsia on 31-May-16.
 */
public class StackoverflowStartPageMap {
    public WebDriver driver;

    public StackoverflowStartPageMap(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@id='tabs']//*[span]/span")
    public WebElement featuredCount;

    @FindBy(xpath = "//*[@id='tell-me-more']")
    public WebElement SignUp;

    @FindBy(xpath = "//a[@class='question-hyperlink']")
    public WebElement question;

    @FindBy(xpath = "//*[@id='hireme']//ul[@class='jobs']/descendant::*[contains(text(),'$')]")
    public List<WebElement> jobOffers;


    public boolean areThereJobsOver100(List<WebElement> list,int arg) {
        List<Integer> intList = new ArrayList<Integer>();
        Pattern pattern = Pattern.compile("[0-9]+");


        for (WebElement s : list) {
            int b = 0;
            Matcher matcher = pattern.matcher(s.getText());
            while (matcher.find()) {
                String a = matcher.group();
                b = Integer.parseInt(a);

            }
            intList.add(b);
        }
        boolean bool = false;
        for (Integer i : intList) {
            if (i >= arg) {
                bool = true;
                break;
            }
        }
        return bool;
    }


    public StackoverflowNetworkPage signUpClick() {
        SignUp.click();
        return new StackoverflowNetworkPage(driver);
    }

    public StackoverflowQuestionPage questionClick() {
        question.click();
        return new StackoverflowQuestionPage(driver);
    }

}
