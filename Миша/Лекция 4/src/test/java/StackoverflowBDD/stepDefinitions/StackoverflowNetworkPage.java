package StackoverflowBDD.stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by varianytsia on 31-May-16.
 */
public class StackoverflowNetworkPage {
public WebDriver driver;

    public StackoverflowNetworkPage(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@id='openid-buttons']//div[contains(@data-provider,'google')]")
    public WebElement Google;

    @FindBy (xpath = "//*[@id='openid-buttons']//div[contains(@data-provider,'facebook')]")
    public WebElement FaceBook;
}
