package StackoverflowBDD.stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by varianytsia on 31-May-16.
 */
public class StackoverflowQuestionPage {
    public WebDriver driver;

    public StackoverflowQuestionPage(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver = driver;
    }

    @FindBy (xpath = "//*[@id='question']//span[@class='relativetime']")
    public WebElement dateOfQuestion;

    public Date getDateOfQuestion() throws ParseException {
        String a = dateOfQuestion.getAttribute("title");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date b = sdf.parse(a);
        return b;
    }
}
