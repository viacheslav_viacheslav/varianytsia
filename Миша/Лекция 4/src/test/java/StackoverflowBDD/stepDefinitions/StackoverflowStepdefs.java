package StackoverflowBDD.stepDefinitions;

import static org.junit.Assert.*;
import StackoverflowBDD.runner.Run;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by varianytsia on 02-Jun-16.
 */
public class StackoverflowStepdefs {
    @Given("^I navigate to Stackoverflow start page$")
    public void iNavigateToStackoverflowStartPage() throws Throwable {
        Run.driver.get(Run.URL);
    }

    @Then("^I see the that featured quantity is more than (\\d+) on start page$")
    public void iSeeTheThatFeaturedQuantityIsMoreThanOnStartPage(int arg0) throws Throwable {
        String a= Run.stackoverflowStartPageMap.featuredCount.getText();
        int b = Integer.parseInt(a);
        assertTrue("Число не более "+arg0,b > arg0);
    }

    @When("^I click on 'Sign Up' button on Stackoverflow start page$")
    public void iClickOnSignUpButton() throws Throwable {
        Run.stackoverflowNetworkPage = Run.stackoverflowStartPageMap.signUpClick();
    }


    @Then("^I see \"([^\"]*)\" buttons on the network page$")
    public void iSeeButtonsOnTheNetworkPage(String arg0) throws Throwable {

        if (arg0.equals("Google")){
        assertTrue("Гугла нету",Run.stackoverflowNetworkPage.Google.isDisplayed());
        }
        else if (arg0.equals("Facebook")){
            assertTrue("Фейсбука нету", Run.stackoverflowNetworkPage.FaceBook.isDisplayed());
        }

    }

    @When("^I click on any question on Stackoverflow start page$")
    public void iClickOnAnyQuestionOnStackoverflowStartPage() throws Throwable {
       Run.stackoverflowQuestionPage = Run.stackoverflowStartPageMap.questionClick();
    }

    @Then("^I see that question has today's date on question page$")
    public void iSeeThatQuestionHasTodaySDateOnQuestionPage() throws Throwable {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        assertEquals("Вопрос был задан не сегодня", sdf2.format(Run.stackoverflowQuestionPage.getDateOfQuestion()), sdf2.format(today));
    }

    @Then("^I see that job offers with more than \\$ (\\d+) K are present on Stackoverflow start page$")
    public void iSeeThatJobOffersWithMoreThan$KArePresentOnStackoverflowStartPage(int arg0) throws Throwable {
        assertTrue("Нету зарплат больше "+ arg0 +" килобаксов",Run.stackoverflowStartPageMap.areThereJobsOver100(Run.stackoverflowStartPageMap.jobOffers, arg0));
    }
}
