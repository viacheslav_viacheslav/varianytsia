package RozetkaBDD.runner;

import RozetkaBDD.stepDefinitions.RozetkaMap;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by varianytsia on 01-Jun-16.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/cucumber-report/rozetka", "json:target/cucumber.json"},
        features = "src/test/java/RozetkaBDD/features",
        glue = "RozetkaBDD/stepDefinitions",
        tags = "@rozetka")

//@NeededTest - запускает 2 теста для розетки
//@rozetka - запускает все тесты розетки

public class Run {

    public static WebDriver driver;
    public static String URL;
    public static RozetkaMap rozetkaMap;

    @BeforeClass
    public static void setUp(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        rozetkaMap = new RozetkaMap(driver);
        URL = "http://rozetka.com.ua/";
//        driver.get(URL);
    }

    @AfterClass
    public static void end(){
        driver.close();
    }
}
