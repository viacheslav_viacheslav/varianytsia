package RozetkaBDD.stepDefinitions;

import RozetkaBDD.runner.Run;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.uk.Нехай;

import static org.junit.Assert.assertTrue;

/**
 * Created by varianytsia on 01-Jun-16.
 */
public class MyStepdefs {
    @Given("^I navigate to Rozetka start page$")
    public void iNavigateToRozetkaStartPage() throws Throwable {
        Run.driver.get(Run.URL);
    }

    @Then("^I see Logo on start page$")
    public void iSeeOnStartPage() throws Throwable {
        assertTrue("Розетка не отображается", Run.rozetkaMap.Logo.isDisplayed());

    }

    @And("^I see \"([^\"]*)\" on start page$")
    public void iSeeOnStartPage(String arg0) throws Throwable {
        if (arg0.equals("Apple")) {
            assertTrue("Розетка не отображается", Run.rozetkaMap.AppleText.isDisplayed());
        } else if (arg0.equals("MP3")) {
            assertTrue("Розетка не отображается", Run.rozetkaMap.MP3.isDisplayed());
        }
    }


    @When("^I click on 'city-picker' on result page$")
    public void iClickOnCityPickerOnResultPage() throws Throwable {
        Run.rozetkaMap.clickCityPicker();
    }

    @Then("^I see the \"([^\"]*)\" are present$")
    public void iSeeTheArePresent(String arg0) throws Throwable {
        if (arg0.equals("Харьков")) {
            assertTrue("Харькова нет", Run.rozetkaMap.Kharkov.isDisplayed());
        } else if (arg0.equals("Киев")) {
            assertTrue("Киева нет", Run.rozetkaMap.Kiev.isDisplayed());
        } else if (arg0.equals("Одесса")) {
            assertTrue("Одессы нет", Run.rozetkaMap.Odessa.isDisplayed());
        }
    }

    @When("^I navigate to the 'Bin' on result page$")
    public void iNavigateToTheBinOnResultPage() throws Throwable {
        Run.rozetkaMap.clickBin();
    }

    @Then("^I see the 'Bin' is empty$")
    public void iSeeTheBinIsEmpty() throws Throwable {
        assertTrue("", Run.rozetkaMap.KorzinaPustaText.isDisplayed());
    }
}
