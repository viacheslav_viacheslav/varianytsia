package RozetkaBDD.stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


/**
 * Created by varianytsia on 30-May-16.
 */
public class RozetkaMap {
    public WebDriver driver;

    public RozetkaMap(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }

    @FindBy(xpath = "//*[@id='body-header']//img[contains(@alt, 'Rozetka.ua')]")
    public WebElement Logo;

    @FindBy (xpath = "//*[@id='m-main']")
    public WebElement AppleText;

    @FindBy (xpath = "//*[@id='m-main']/li[3]/a")
    public  WebElement MP3;

    @FindBy (xpath = "//*[@id='city-chooser']/a")
    public WebElement CityPicker;

    @FindBy (xpath = "//*[@id='city-chooser']//*[contains(@class,'header-city')][contains(text(),'Харьков')]")
    public WebElement Kharkov;

    @FindBy (xpath = "//*[@id='city-chooser']//*[contains(@class,'header-city')][contains(text(),'Киев')]")
    public WebElement Kiev;

    @FindBy (xpath = "//*[@id='city-chooser']//*[contains(@class,'header-city')][contains(text(),'Одесса')]")
    public WebElement Odessa;

    @FindBy (xpath = "//a[@class='sprite-side novisited hub-i-link hub-i-cart-link']")
    public WebElement Bin;

    @FindBy (xpath = "//*[@id='drop-block']/h2")
    public WebElement KorzinaPustaText;

    public RozetkaMap clickCityPicker(){
        CityPicker.click();
        return new RozetkaMap(driver);
    }

    public RozetkaMap clickBin(){
        Bin.click();
        return new RozetkaMap(driver);
    }





}
