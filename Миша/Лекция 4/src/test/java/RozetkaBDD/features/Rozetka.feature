@rozetka
Feature: check the elements on Rozetka website

  Background:
    Given I navigate to Rozetka start page

  @NeededTest
  Scenario: 001 Checking the Logo
    Then I see Logo on start page

  @NeededTest
  Scenario Outline: 002 Checking Apple and MP3
    Then I see "<element>" on start page

    Examples:
      | element |
      | MP3   |
      | Apple   |

  Scenario Outline: 003 Checking the cities
    When I click on 'city-picker' on result page
    Then I see the "<cities>" are present

    Examples:
      | cities  |
      | Харьков |
      | Киев    |
      | Одесса  |

    Scenario: 004 Checking the bin
      When I navigate to the 'Bin' on result page
      Then I see the 'Bin' is empty
