/**
 * Created by varianytsia on 05-Apr-16.
 */

// Лекция 2
public class Lection2_task1_2_3
{
    public static void main(String args[] )
    {
        int a = 7;
        int b = 5;

        //первое задание
        System.out.println("Первое задание");
        System.out.println("Сложение a+b = " + (a+b));
        System.out.println("Умножение a*b = " + (a*b));
        System.out.println("Вычитание a-b = " + (a-b));
        System.out.println("Деление a/b = " + (float)a/b);
        System.out.println("Остаток от деления a%b = " + (a%b));
        System.out.println("Сравнение a == b " + (a==b));
        System.out.println("Сравнение a <= b " + (a<=b));
        System.out.println("Сравнение a >= b " + (a>=b));
        System.out.println("Сравнение a > b " + (a>b));
        System.out.println("Сравнение a < b " + (a<b));
        System.out.println("Сравнение a != b " + (a!=b));
        System.out.println("Логические a==7 && b==8 " + (a==7 && b==8));
        System.out.println("Логические a==7 || b==8 " + (a==7 || b==8));

        //второе задание
        byte c = 127;
        String c1 = Byte.toString(c);
        byte c2 = Byte.parseByte(c1);

        short d = 32767;
        String d1 = Short.toString(d);
        short d2 = Short.parseShort(d1);

        int e = 30;
        String e1 = Integer.toString(e);
        int e2 = Integer.parseInt(e1);

        long f = 1012L;
        String f1 = Long.toString(f);
        long f2 = Long.parseLong(f1);

        float g = 45.3F;
        String g1 = Float.toString(g);
        float g2 = Float.parseFloat(g1);

        double h = 4546.3666D;
        String h1 = Double.toString(h);
        double h2 = Double.parseDouble(h1);

        boolean i = true;
        String i1 = Boolean.toString(i);
        boolean i2 = Boolean.parseBoolean(i1);

        char j = '\u0169';
        String j1 = Character.toString(j);
        char j2 = j1.charAt(0);

        System.out.println();
        System.out.println();
        System.out.println("Второе задание");
        System.out.println("Перевод byte = " + c+ " to String = " + c1);
        System.out.println("Перевод String = " + c1+ " to Byte = " + c2);

        System.out.println();
        System.out.println("Перевод short = " + d+ " to String = " + d1);
        System.out.println("Перевод String = " + d1+ " to Short = " + d2);

        System.out.println();
        System.out.println("Перевод int = " + e+ " to String = " + e1);
        System.out.println("Перевод String = " + e1+ " to int = " + e2);

        System.out.println();
        System.out.println("Перевод long = " + f+ " to String = " + f1);
        System.out.println("Перевод String = " + f1+ " to long = " + f2);

        System.out.println();
        System.out.println("Перевод float = " + g+ " to String = " + g1);
        System.out.println("Перевод String = " + g1+ " to float = " + g2);

        System.out.println();
        System.out.println("Перевод double = " + h+ " to String = " + h1);
        System.out.println("Перевод String = " + h1+ " to double = " + h2);

        System.out.println();
        System.out.println("Перевод boolean = " + i+ " to String = " + i1);
        System.out.println("Перевод String = " + i1+ " to boolean = " + i2);

        System.out.println();
        System.out.println("Перевод char = " + j+ " to String = " + j1);
        System.out.println("Перевод String = " + j1+ " to char = " + j2);


        //третье задание

        Float k = 100.236564F;
        int n = k.intValue();
        float m = n;

        System.out.println();
        System.out.println();
        System.out.println("Третье задание");
        System.out.println("Перевод Float = " + k + " to int = " + n);
        System.out.println("Перевод int = " + n + " to float = " + m);




    }
}
