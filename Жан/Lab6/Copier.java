package Lection6;

import java.io.*;

/**
 * Created by Славон on 02.05.2016.
 */
public class Copier {
    public static void copyingFiles(File sourceFile, File destinationFile) throws IOException {
        InputStream input = null;
        OutputStream output = null;
        try {
            input = new FileInputStream(sourceFile);
            output = new FileOutputStream(destinationFile);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("ошибка в процессе копирования");
        }
        finally {
            input.close();
            output.close();
        }
    }
}