package Lection4;

/**
 * Created by varianytsia on 12-Apr-16.
 */
public class Car2 extends Car {
    final double initialAcceleration;

    public Car2(String name, double maxSpeed, double acceleration, double mobility) {
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        this.mobility = mobility;
        initialAcceleration = acceleration;
        this.name = name;
    }

    @Override
    public void carFeatureIs() {
        acceleration = initialAcceleration;
        if (speed < maxSpeed / 2) {
            this.acceleration = 2 * this.acceleration;
        }
        speedChangeMobility();

    }


}
