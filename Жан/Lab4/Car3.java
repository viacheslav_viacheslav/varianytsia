package Lection4;

/**
 * Created by varianytsia on 12-Apr-16.
 */
public class Car3 extends Car {
    public Car3(String name, double maxSpeed, double acceleration, double mobility) {
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        this.mobility = mobility;
        this.name = name;
    }

    @Override
    public void carFeatureIs() {
        if (speed == maxSpeed) {
            this.maxSpeed = 1.1 * this.maxSpeed;
        }
        speedChangeMobility();

    }


}

