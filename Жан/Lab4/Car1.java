package Lection4;

import static java.lang.Math.abs;

/**
 * Created by varianytsia on 12-Apr-16.
 */
public class Car1 extends Car {
    double initialMobility;

    public Car1(String name, double maxSpeed, double acceleration, double mobility) {
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        this.mobility = mobility;
        this.name = name;

    }

    @Override
    public void carFeatureIs() {
        if (speed > maxSpeed / 2) {
            mobility = mobility + 0.05 * abs(maxSpeed / 2 - speed);
        }
        speedChangeMobility();
        this.initialMobility = mobility;
    }


}
