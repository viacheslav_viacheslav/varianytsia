package Lection4;

/**
 * Created by varianytsia on 12-Apr-16.
 */
public abstract class Car {

    double speed;
    double maxSpeed;
    double acceleration;
    double mobility;
    double timeTotal;
    double time;
    String name;

    public void linearMovement() {
        this.speed = Math.sqrt((this.speed * this.speed) + 2 * this.acceleration * 12960 * 2);
    }


    public void speedChangeMobility() {
        speed = mobility * speed;
        if (speed > maxSpeed) {
            speed = maxSpeed;
        }
        if (mobility > 1) {
            mobility = 1;
        }

    }

    public static String timeConverter(double timeTotal) {
        String convertedTime = (int) (timeTotal * 60) + " мин " + (int) (timeTotal * 3600 - ((int) (timeTotal * 60)) * 60) + " сек";
        return convertedTime;
    }

    public abstract void carFeatureIs();

    public double getTime() {
        time = 2 / this.speed;
        return time;
    }


    public void intermediateState(int i) {
        System.out.println(this.name + ":" + i + " поворот: " + "скорость: " + (int) (speed) + " км/ч " + " ускорение: " + Math.rint(100.0 * acceleration) / 100.0 + " м/с^2 " + " маневренность: " + Math.rint(100.0 * mobility) / 100.0 + " время: " + (int) (time * 60) + " мин " + (int) (time * 3600 - ((int) (time * 60)) * 60) + " сек");
    }

    public void timeTotalDeclaration(int i) {
        System.out.println(this.name + ": " + timeConverter(timeTotal));
    }

}
