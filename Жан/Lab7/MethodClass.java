package Lection7;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by varianytsia on 06-May-16.
 */
public class MethodClass
{

    public void methodSeri(Child object) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        Method seriMethod = Child.class.getSuperclass().getDeclaredMethod("serializer");
        seriMethod.setAccessible(true);
        seriMethod.invoke(object);
    }

    public Object methodDeseri() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        Method deserMethod = Child.class.getSuperclass().getDeclaredMethod("deserializer", String.class);
        deserMethod.setAccessible(true);
        //Child deserObject = new Child();
        return deserMethod.invoke(new Child(),"D:\\Java\\Учебная папка\\seri.txt");
    }

    public void changingTheState(Child object) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException
    {
        Method changingState = Child.class.getSuperclass().getDeclaredMethod("changeState");
        changingState.setAccessible(true);
        changingState.invoke(object);
    }
}
