package Lection7;

import junit.framework.TestCase;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class MethodClassTest extends TestCase
{

    @Test
    public void testMethodSeri() throws Exception
    {
        Child asf = new Child();
        boolean state = false;
        File file = new File("D:\\Java\\Учебная папка");
        file.mkdir();


        MethodClass obj = new MethodClass();
        obj.methodSeri(asf);

        File[] files = file.listFiles();
        for (File a : files)
        {
            if (a.getName().equals("seri.txt"))
            {
                state = true;
            }
        }

        assertTrue(state);


    }

    @Test
    public void testMethodDeseri() throws Exception
    {
        //       Child asd = new Child();
        MethodClass obj = new MethodClass();
        String a = obj.methodDeseri().getClass().getName();
        String b = Child.class.getName();
        assertEquals(a, b);
    }

    @Test
    public void testChangingTheState() throws Exception
    {
        Child asf = new Child();
        Class cla = asf.getClass();
        Field field = cla.getSuperclass().getDeclaredField("f");
        field.setAccessible(true);
//      String a = (String)field.get(asf);
        //System.out.println(a);
        MethodClass obj = new MethodClass();
        obj.changingTheState(asf);
//        Child asd2 = new Child();
//        Class cla2 = asf.getClass();
//        Field field2 = cla2.getSuperclass().getDeclaredField("f");
//        field2.setAccessible(true);
        String b = (String) field.get(asf);
        assertEquals("Закончилась картошка", b);
    }
}