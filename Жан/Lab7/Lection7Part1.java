package Lection7;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by varianytsia on 05-May-16.
 */
public class Lection7Part1 {
    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        Child asd = new Child();
        MethodClass aa = new MethodClass();//
//        Class cla = asd.getClass();
//        Method seriMethod = cla.getSuperclass().getDeclaredMethod("serializer");
//        seriMethod.setAccessible(true);
//        seriMethod.invoke(asd);
        aa.methodSeri(asd);//
//        Method deserMethod = cla.getSuperclass().getDeclaredMethod("deserializer", String.class);
//        deserMethod.setAccessible(true);
//        Child deserObject = new Child();
//        deserMethod.invoke(deserObject, "D:\\Java\\Учебная папка\\seri.txt");
        MethodClass bb = new MethodClass(); //
        bb.methodDeseri();

//        Field field = cla.getSuperclass().getDeclaredField("f");
//        field.setAccessible(true);

//        Method changingState = cla.getSuperclass().getDeclaredMethod("changeState");
//        changingState.setAccessible(true);
//        changingState.invoke(asd);
        bb.changingTheState(asd);


    }
}
