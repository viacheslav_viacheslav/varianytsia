package Lection5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static Lection5.AllTheOther.checkTheDate;
import static Lection5.AllTheOther.dateInsurance;


/**
 * Created by varianytsia on 14-Apr-16.
 */


public class DateList {
    public static void main(String[] args) throws AllTheOther.MyException {


        MyDateList list = new MyDateList();


        for (int i = 0; i <=30; i++) {
            Random randomiser = new Random();
            int a = randomiser.nextInt(2016);
            int b = randomiser.nextInt(12);
            int c = randomiser.nextInt(31);
            LocalDate myDate = LocalDate.of(dateInsurance(a), dateInsurance(b), dateInsurance(c));
            list.add(myDate);
        }


        for (int i = 0; i < list.size(); i++) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE dd MMM YYYY");
            System.out.println(formatter.format((LocalDate) list.get(i)) + " " + checkTheDate((LocalDate) list.get(i)));
        }


    }






}


