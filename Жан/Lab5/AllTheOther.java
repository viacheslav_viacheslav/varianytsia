package Lection5;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by varianytsia on 25-Apr-16.
 */
public class AllTheOther {
    public static int dateInsurance(int a) {
        if (a == 0) {
            a = 1;
        } else if (a == 29 || a == 30 || a == 31) {
            a = 28;
        }
        return a;
    }

    public static String checkTheDate(LocalDate myDate) throws MyAnotherException, MyException {
        String date1;
        date1 = myDate.format(DateTimeFormatter.ofPattern("E"));
        if (date1.equals("Sun") || date1.equals("Sat")) {
            try {
                throw new MyAnotherException();
            }
            catch (MyAnotherException e) {
                return "- Выходной день!";
            }
        } else
            try {
                throw new MyException();
            }
            catch (MyException e) {
                return "- Будний день!";
            }
    }

    public static class MyException extends Exception {
        public MyException() {
            super();
        }
    }

    public static class MyAnotherException extends RuntimeException {
        public MyAnotherException() {
            super();
        }
    }
}
