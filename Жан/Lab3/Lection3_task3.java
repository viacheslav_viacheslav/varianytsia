import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

/**
 * Created by varianytsia on 08-Apr-16.
 */
public class Lection3_task3 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите конечную границу диапазона");

        String input = reader.readLine();
        int border = Integer.parseInt(input);
        Date start = new Date();
        long startLong = start.getTime();
        for (int i = 1; i < border; i=i+2) {
            boolean status = true;
            for (int j = 2; j < i/2+1; j++) {
                if (i % j == 0) {
                    status = false;
                    break;
                }

            }
            if (status){
                System.out.println((i==1 ? i+1 : i));
            }


//              int counter = 0;
//            for (int j = 1; j <= i; j++) {
//                if (i % j == 0) {
//                    counter++;
//                    if (counter == 3) //если закомментить эти строки,то можно будет увидеть ,что время ,затраченное на операцию,увеличится
//                        break;        // в 8 раз
//                }
//
//            }
//            if (counter < 3)
//                System.out.println(i);
        }

        Date finish = new Date();
        long finishLong = finish.getTime();
        System.out.println();
        System.out.println("Затраченное время на операцию  " + (finishLong - startLong) + " мс");

    }
}
