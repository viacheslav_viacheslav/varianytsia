import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Created by varianytsia on 08-Apr-16.
 */
public class Lection3_task1
{
    public static void main(String[] args) throws IOException
    {

        int[] array = new int[4];

        for (int i = 0; i < array.length; i++)
        {
            Random rand = new Random();

            array[i] = rand.nextInt();

            System.out.println(array[i]);
        }

        int min = array[0];
        for (int i = 0; i < array.length; i++)
        {

            if (min > array[i])
            {
                min = array[i];
            }

        }

        System.out.println("Вот вам наименьшее число " + min);

    }
}
