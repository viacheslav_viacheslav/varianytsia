import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by varianytsia on 08-Apr-16.
 */
public class Lection3_task5 {
    public static void main(String[] args) throws IOException {
        System.out.println("Вводите числа до посинения");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = null;
        float sum = 0;
        String stop = "сумма";
        while (true) {
            input = reader.readLine();
            try {
                if (input.equals(stop)) {
                    break;
                }
                float memberOfArray = Float.parseFloat(input);
                sum += memberOfArray;
                // a = memberOfArray;
            }
            catch (Exception e) {
                System.out.println("Введите число");
            }

        }
        if (sum % 1 == 0) {
            System.out.println((int) sum);
        }
        else System.out.println(sum);
    }
}
