package PIP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by varianytsia on 01-Jul-16.
 */
public class APISources {
    URL url;
    InputStream in;
    String output;


    public InputStream makeConnection(String line) throws IOException {
        url = new URL(line);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        in = connection.getInputStream();
        return in;
    }

    public String read(String line) {
        Pattern p1 = Pattern.compile("\\s\\s\\s.+");
        Matcher m1 = p1.matcher(line);
        String a = null;
        if (m1.find()) {
            a = m1.group().substring(3);
        }
        return a;

    }

    public Matcher match(String a, BufferedReader reader2) throws IOException {
        Pattern p2 = Pattern.compile(a);
        String temp = "";
        while ((output = reader2.readLine()) != null) {
            temp += output;
        }
        System.out.println(temp);
        Matcher m2 = p2.matcher(temp);
        return m2;
    }
}
