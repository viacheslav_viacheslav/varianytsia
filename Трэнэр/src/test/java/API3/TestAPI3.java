package API3;

import org.junit.Assert;
import org.junit.Test;

import PIP.APISources;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by varianytsia on 24-Jun-16.
 */
public class TestAPI3 {


    @Test
    public void TestAPI3() {
        InputStream in;
        int counter = 0;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File("D:\\Java\\List.txt")));
            String line = reader.readLine();

            while (line != null) {
                APISources apis = new APISources();
                in = apis.makeConnection(line);
                BufferedReader reader2 = new BufferedReader(new InputStreamReader(in));
                String a = apis.read(line);

                try {
                    counter++;
                    Assert.assertTrue(apis.match(a, reader2).find());

                } catch (AssertionError e) {
                    System.out.println("Test #" + counter + " Failed");
                } finally {
                    line = reader.readLine();
                    continue;
                }

            }
        } catch (FileNotFoundException e) {

        } catch (IOException e) {

        }
    }


}

